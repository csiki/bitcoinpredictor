import tensorflow as tf
import blockchain_api
from tensorflow.models.rnn import rnn
from tensorflow.models.rnn.rnn_cell import BasicLSTMCell, LSTMCell
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt


class Config(object):  # hyperparameters
    """ Config. """
    init_scale = 0.1  # the initial scale of the weights
    learning_rate = 1.0  # the initial value of the learning rate
    max_grad_norm = 5  # the maximum permissible norm of the gradient
    num_layers = 2  # the number of LSTM layers
    num_steps = 20  # the number of unrolled steps of LSTM
    hidden_size = 1  # the number of LSTM units
    max_epoch = 4  # the number of epochs trained with the initial learning rate
    max_max_epoch = 13  # the total number of epochs for training
    keep_prob = 1.0  # the probability of keeping weights in the dropout layer
    lr_decay = 0.5  # the decay of the learning rate for each epoch after "max_epoch"
    batch_size = 1  # number of inputs processed at once
    seq_width = 4  # number of variables as input


# get data
price, tradevol, txvol, ntx = blockchain_api.get_blockchain_data()

# normalize data
price_norm = norm(price)
tradevol_norm = norm(tradevol)
txvol_norm = norm(txvol)
ntx_norm = norm(ntx)
price /= price_norm
tradevol /= tradevol_norm
txvol /= txvol_norm
ntx /= ntx_norm
x = zip(price[:-1], tradevol[:-1], txvol[:-1], ntx[:-1])
x = np.array([np.asarray(t) for t in x])
y_ = price[1:]


# LSTM model
# c = Config()
# c.num_steps = len(x)
# seq_input = tf.placeholder(tf.float32, [c.num_steps, c.seq_width])
# inputs = [tf.reshape(i, (c.batch_size, c.seq_width)) for i in tf.split(0, c.num_steps, seq_input)]
# lstm_cell = rnn.rnn_cell.LSTMCell(c.hidden_size, c.seq_width)
# if c.keep_prob < 1:
#     lstm_cell = rnn.rnn_cell.DropoutWrapper(
#         lstm_cell, output_keep_prob=c.keep_prob)
# #cell = rnn.rnn_cell.MultiRNNCell([lstm_cell] * c.num_layers)
#
# initial_state = lstm_cell.zero_state(c.batch_size, tf.float32)
# outputs, states = rnn.rnn(lstm_cell, inputs, initial_state=initial_state)



# simple LSTM model
c = Config()
seq_input = tf.placeholder(tf.float32, c.seq_width)
desired_output = tf.placeholder(tf.float32)
lstm_cell = rnn.rnn_cell.LSTMCell(c.hidden_size, c.seq_width)
lstm_cell()


# training
with tf.Session() as session:
    session.run(tf.initialize_all_variables())
    feed = {seq_input: x}
    outputs = session.run(outputs, feed_dict=feed)
    print len(outputs), len(outputs[0])
    print np.mean(outputs[0]), np.mean(outputs[-1])
